package presentation.exceptions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import presentation.laziness.DataType;

@Value
@Builder
@RequiredArgsConstructor
public class DataFileMetadata {

    @NonNull Long     customerId;
    @NonNull DataType type;
    @NonNull File     file;

    private Option<Either<IOException, String>> contents = Option.none();

    public Either<IOException, String> getContents() {
        return contents.getOrElse(this::loadContents);
    }

    private Either<IOException, String> loadContents() {
        try {
            return Either.right(new String(Files.readAllBytes(file.toPath())) );
        } catch (IOException e) {
            return Either.left(e);
        }
    }

}