package presentation.exceptions;


import java.io.IOException;

import io.vavr.collection.List;
import io.vavr.control.Either;
import presentation.composition.functional.Data;
import presentation.composition.functional.Record;
import presentation.laziness.DataFileUnavailableException;

public class CustomerService {
    
    public List<Either<IOException, Data>> 
               loadDataAndReturnAllErrors(
                       List<DataFileMetadata> fileInfo) {
        
        return fileInfo.map(metadata -> 
            metadata
                .getContents()
                .map(this::_buildRecords)
                .map(records -> new Data(metadata.getCustomerId(), records))
        );
    }
    
    public List<Data> loadDataOrThrow(List<DataFileMetadata> fileInfo) {
        return fileInfo.map(metadata -> 
            metadata
                .getContents()
                .map(this::_buildRecords)
                .map(records -> new Data(metadata.getCustomerId(), records))
                .getOrElseThrow( e -> new DataFileUnavailableException(e) )
        );
    }
    
    public List<Data> loadDataOrEmpty(List<DataFileMetadata> fileInfo) {
        
        return fileInfo.map(metadata ->
            new Data(
                 metadata.getCustomerId(), 
                 metadata
                    .getContents()
                    .map(this::_buildRecords)
                    .getOrElse(List.empty())
            )
        );

    }
    
    // Create Records (domain-specific object level)
    //
    private List<Record> _buildRecords(String contents) {
        return _splitIntoRecords(contents).map(idAndData -> 
            new Record(Long.valueOf(idAndData[0]), idAndData[1])
        );
    }
    
    // Handles low-level splitting
    //
    private List<String[]> _splitIntoRecords(String contents) {
        return List.of(contents.split(",")).map(recordStr -> recordStr.split(":"));
    }
    
}
