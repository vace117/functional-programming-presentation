package presentation.structure;
import static presentation.util.Utils.print;

/**
 * This demonstrates the standard OOP approach, where the methods on the objects
 * modify shared state 
 */
public class StructureWithSharedState {
    // Shared state
    //
    private Integer number = 100;
    private String  description = "One Hundred";
    
    public void imperativeStyle() {
        _doThis();
        _doThat();
        _doSomethingElse();
        _printResult();
    }
    
    
    
    private void _doThis()          { number = number / 10; }
    private void _doThat()          { description = "Ten"; }
    private void _doSomethingElse() { description += "Ten (after dividing by 10)"; }
    private void _printResult()     { print(number + ", " + description); }

    public static void main(String[] args) {
        new StructureWithSharedState().imperativeStyle();
    }
}
