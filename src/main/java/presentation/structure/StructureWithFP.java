package presentation.structure;
import static presentation.util.Utils.print;

import presentation.domain.Example1;

/**
 * This demonstrates the standard FP approach, where the methods are 
 * Referentially Transparent, and the side-effect is explicit
 */
public class StructureWithFP {
    
    public void functionalStyle() {
        _printResult( // This is the side-effect
            
            // This is the "pure", referentially-transparent part
            //
            _doSomethingElse(
                _doThat(
                    _doThis( new Example1(100, "One Hundred") )
                )
            )
        );
    }
        
    private Example1 _doThis(Example1 in) { 
        return in.withNumber(in.getNumber() / 10); 
    }
    private Example1 _doThat(Example1 in) { 
        return in.withDescription("Ten"); 
    }
    private Example1 _doSomethingElse(Example1 in) { 
        return in.withDescription(in.getDescription() + " (after dividing by 10)"); 
    }
    private void     _printResult(Example1 in) { 
        print(in.toString()); 
    }

    public static void main(String[] args) {
        new StructureWithFP().functionalStyle();
    }
    
}
