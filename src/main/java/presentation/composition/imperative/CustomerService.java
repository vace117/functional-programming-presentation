package presentation.composition.imperative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerService {
    public List<Data> loadData(List<DataFileMetadata> fileInfo) {
        List<Data> result = new ArrayList<>();

        for (DataFileMetadata metadata : fileInfo) {
            // Records are comma-separated
            List<String> contentRecords = Arrays.asList(metadata.getContents().split(","));
            List<Record> records = new ArrayList<>();
            
            for (String recordStr : contentRecords) {
                // Record fields are ':' separated
                String[] idAndData = recordStr.split(":");
                Record record = new Record(Long.valueOf(idAndData[0]), idAndData[1]);
                records.add(record);
            }

            result.add(new Data(metadata.getCustomerId(), records));
        }
        
        return result;
    }
}

