package presentation.composition.imperative;

import java.util.List;

import lombok.Value;

@Value // Immutable
public class Data {
    private long customerId;
    private List<Record> contents;
}

