package presentation.composition.imperative;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import io.vavr.Function0;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import presentation.laziness.DataFileUnavailableException;
import presentation.laziness.DataType;

@Value
@Builder
@RequiredArgsConstructor
public class DataFileMetadata {

    @NonNull       Long     customerId;
    @NonNull       DataType type;
    @NonNull @With File     file;

    private Function0<String> contents = Function0
                                            .of(this::loadContents)
                                            .memoized(); 

    public String getContents() {
        return contents.apply();
    }

    private String loadContents() {
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new DataFileUnavailableException(e);
        }
    }

}

