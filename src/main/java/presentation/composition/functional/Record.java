package presentation.composition.functional;

import lombok.Value;

@Value // Immutable
public class Record {
    private long   recordId;
    private String record;
}
