package presentation.composition.functional;


import io.vavr.collection.List;
import presentation.composition.imperative.DataFileMetadata;

public class CustomerService1 {
    
    public List<Data> loadData(List<DataFileMetadata> fileInfo) {
        
        return fileInfo.map(metadata ->
            new Data(metadata.getCustomerId(),
                List.of(metadata.getContents().split(","))
                    .map(recordsStr -> recordsStr.split(":"))
                    .map(idAndData ->
                        new Record(Long.valueOf(idAndData[0]), idAndData[1])
                    )
            )
        );
        
    }

}
