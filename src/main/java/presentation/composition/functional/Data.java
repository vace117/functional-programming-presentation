package presentation.composition.functional;

import io.vavr.collection.List;
import lombok.Value;

@Value // Immutable
public class Data {
    private long         customerId;
    private List<Record> contents;
    
}
