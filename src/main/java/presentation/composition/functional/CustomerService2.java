package presentation.composition.functional;


import io.vavr.collection.List;

public class CustomerService2 {
    public List<Data> loadData(List<DataFileMetadata> fileInfo) {
        
        // We say what we would like to happen, not how it should happen. 
        // We can easily replace the execution model to run the code in different ways 
        // (sequentially on a single thread or asynchronously or in parallel, etc).
        //
        return fileInfo.map(metadata ->
            metadata
                .getContents()
                .map(this::_buildRecords)
                .map(records -> new Data(metadata.getCustomerId(), records))
                .getOrElseThrow( e -> new RuntimeException(e) )
        );
        
    }
    
    // Create Records (domain-specific object level)
    //
    private List<Record> _buildRecords(String contents) {
        return _splitIntoRecords(contents).map(idAndData -> 
            new Record(Long.valueOf(idAndData[0]), idAndData[1])
        );
    }
    
    // Handles low-level splitting
    //
    private List<String[]> _splitIntoRecords(String contents) {
        return List.of(contents.split(",")).map(recordStr -> recordStr.split(":"));
    }
}
