package presentation.composition.functional;

import java.io.File;
import java.nio.file.Files;

import io.vavr.Function0;
import io.vavr.control.Try;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import presentation.laziness.DataType;

@Value
@Builder
@RequiredArgsConstructor
public class DataFileMetadata {

    @NonNull Long     customerId;
    @NonNull DataType type;
    @NonNull File     file;

    private Function0<Try<String>> contents = Function0
                                                  .of(this::loadContents)
                                                  .memoized();

    public Try<String> getContents() {
        return contents.apply();
    }

    private Try<String> loadContents() {
        return Try.of( () -> new String(Files.readAllBytes(file.toPath())) );
    }

}