package presentation.mutable;

import java.io.File;

import cyclops.control.Option;
import presentation.composition.imperative.DataFileMetadata;
import presentation.laziness.DataFileMetadataLazy;
import presentation.laziness.DataType;

public class DataFileService {
    private UserService    userService;
    private DataFileWriter writer;

    public DataFileMetadataLazy createDataFileMetadata1(
                User user, DataType type, String contents, String location) {
        
        // Declared and then instantiated via a No-Argument constructor.
        // None of the fields have been set.
        //
        DataFileMetadataLazy metadata = new DataFileMetadataLazy();

        // Mutated inside one or maybe even both methods where it is provided as an
        // input parameter.
        // This makes reuse of DataFileWriter::writeDataFile much more risky and less
        // likely
        //
        userService.attachCustomerInfo(metadata, user, type);
        writer.writeDataFile(metadata, contents, location);

        // So is this actually a properly initialized instance of DataFileMetadataLazy?
        // Maybe... we have to understand everything that happened inside the 2 methods
        // above in
        // order to answer that. Unnecessary complexity again!
        //
        return metadata;
    }

    // Another common pattern of initializing mutable objects
    //

    public DataFileMetadataLazy createDataFileMetadata2(
                Long customerId, DataType type, String contents) {

        DataFileMetadataLazy metadata = new DataFileMetadataLazy();
        
        metadata.setCustomerId(customerId);
        if (contents != null) {
            File output = save(contents);
            metadata.setFile(output);
            metadata.setType(type);
        } else {
            metadata.setType(DataType.EMPTY_TYPE);
        }
        
        return metadata;
    }
    
    // With immutable Objects the key insight is that we need to do the opposite of what we did above. 
    // We need to make all of our fields values available prior to declaration and initialization,
    // or we can use expressions instead of statements to initialize our object in-place.
    //
    public DataFileMetadata createDataFileMetadataImmutable1(Long customerId, DataType type, String contents) {
        return new DataFileMetadata(
            customerId, 
            contents == null ? DataType.EMPTY_TYPE : type, // Expression instead of a Statement
            save(contents)
        );
    }
    
    // If our object had a lot of fields, it would be rather cumbersome to use a huge constructor.
    // Instead, we would use the builder
    //
    public DataFileMetadata createDataFileMetadataImmutable2(Long customerId, DataType type, String contents) {
        return DataFileMetadata
            .builder()
                .customerId ( customerId )
                .type       ( contents == null ? DataType.EMPTY_TYPE : type )
                .file       ( save(contents) )
            .build();
    }
    
    // We can't update immutable objects, so we make a copy using the copy constructor
    //
    public DataFileMetadata updateImmutable1(DataFileMetadata metadata, String newContents) {
        return new DataFileMetadata(
            metadata.getCustomerId(), 
            metadata.getType(), 
            save(newContents)
        );
    }

    // We can't update immutable objects, so we make a copy using our fancy 'with....()' method
    //
    public DataFileMetadata updateImmutable2(DataFileMetadata metadata, String newContents) {
        return metadata.withFile( save(newContents) );
    }

    private File save(String contents) {
        // TODO Auto-generated method stub
        return null;
    }

}
