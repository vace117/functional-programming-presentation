package presentation.mutable;

import presentation.laziness.DataFileMetadataLazy;
import presentation.laziness.DataType;

public class UserService {

    private CustomerDAO dao;

    // Mutability is a bad idea b/c it doesn’t guide us away from writing code like
    // this.
    //
    // The void return type means we don’t have to return a DataFileMetadata
    // instance (because we can mutate in place). So there is nothing to alert us that 
    // we’re creating execution paths where we can completely fall through w/o actually 
    // setting up the DataFileMetadataLazy properly.
    //
    // Perhaps the right thing to do is throw an Exception. 
    // The absence of controlled error handling here, means that NullPointerExceptions 
    // elsewhere are very likely.
    //
    public void attachCustomerInfo(DataFileMetadataLazy meta, User user, DataType type) {
        if (dao.isSupportedType(user, type)) {
            meta.setCustomerId(dao.getCustomerId(user));
            meta.setType(type);
        }
    }

}
