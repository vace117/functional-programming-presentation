package presentation.domain;
import java.io.IOException;

import lombok.Value;
import lombok.With;

@Value
public class Example1 {
    @With Integer number;
    @With String  description;
    
    public void saveToDatabase() {
        try {
            _persist();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void saveToDatabase_withCheckedException() throws IOException {
        _persist();
    }
    
    private void _persist() throws IOException {
        if ( number == 2 ) throw new IOException("Database error!");
    }
    
}

