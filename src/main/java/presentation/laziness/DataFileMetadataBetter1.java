package presentation.laziness;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import io.vavr.control.Option;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataFileMetadataBetter1 {

    private Long   customerId;
    private String type;
    private File   file;
    private String contents;

    // We have successfully encapsulated the file management aspect of the class 
    // and fixed the performance issue by introducing laziness.
    //
    // But this is still rather verbose, b/c we are still using the imperative code style.
    // We can clean this code up by introducing some functional types...
    //
    public String getContents() {
        if (contents == null) {
            loadContents();
        }
        
        return contents;
    }

    private void loadContents() {
        try {
            contents = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new DataFileUnavailableException(e);
        }
    }

}