package presentation.laziness;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.function.Supplier;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataFileMetadataBetter2 {

    private Long           customerId;
    private DataType       type;
    private File           file;
    
    // The JDK’s Supplier is a Functional Interface that represents
    // something that can be supplied to us LAZILY in the future.
    //    
    private Supplier<String> contents = this::loadContents;

    public String getContents() {
        return contents.get();
    }

    private String loadContents() {
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new DataFileUnavailableException(e);
        }
    }

}