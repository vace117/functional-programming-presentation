package presentation.laziness;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import io.vavr.control.Option;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataFileMetadataBadness1 {

    private Long   customerId;
    private String type;
    private File   file;
    private String contents;

    // There is an inter-dependency between the getContents method 
    // and the loadContents method that we’ve left for client code to resolve.
    //
    public void loadContents(){
        try {
            contents = new String(Files.readAllBytes(file.toPath()));
        }
        catch(IOException e) {
            throw new DataFileUnavailableException(e);
        }
    }
    
}



