package presentation.laziness;

import java.util.HashMap;
import java.util.Map;

public class FileManager {
    private Map<Long, String> customerFileMap = new HashMap<>();

    public void process(DataFileMetadataBadness1 metadata) {
        // Depending on when FileManager::process is called, we might end up
        // with nulls in our map. We have to fix it...
        // 
        // Option 1: Call metadata.loadContents() in this method
        //
        // Option 2: Call this method after we think metadata.loadContents() 
        //           was already called
        //
        // Option 3: Maybe we need to check first?
        //      if(metadata.getContents()==null) metadata.loadContents();
        //
        // Option 4: We might get lucky and it will just work w/o explicitly 
        //           thinking about all this. For now...
        //
        customerFileMap.put(metadata.getCustomerId(), metadata.getContents());
    }
}

