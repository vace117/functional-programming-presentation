package presentation.laziness;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import io.vavr.control.Option;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataFileMetadataBetter0 {

    private Long   customerId;
    private String type;
    private File   file;
    private String contents = loadContents();


    private String loadContents() {
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new DataFileUnavailableException(e);
        }
    }

}

