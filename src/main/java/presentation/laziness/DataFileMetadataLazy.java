package presentation.laziness;

import java.io.File;
import java.nio.file.Files;

import io.vavr.Function0;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataFileMetadataLazy {

    private Long           customerId;
    private DataType       type;
    private File           file;
    
    private Function0<String> contents = Function0.of(this::loadContents).memoized();

    public String getContents() {
        return contents.apply();
    }

    private String loadContents() {
        try {
            return new String( Files.readAllBytes(file.toPath()) );
        } catch (Exception e) {
            throw new DataFileUnavailableException(e);
        }
    }

}