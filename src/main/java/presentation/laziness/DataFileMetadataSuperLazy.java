package presentation.laziness;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import cyclops.control.Eval;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DataFileMetadataSuperLazy {

    private Long         customerId;
    private String       type;
    private File         file;

    // This is even better than Function0<String> b/c its even more explicit
    // about the fact that having no content is not really a valid state.
    // It clearly states that it's not ok for this field to be empty -
    // it just needs initialization before it can be used.
    //
    private Eval<String> contents = Eval.later(this::loadContents);

    public String getContents() {
        return contents.get();
    }

    private String loadContents() {
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new DataFileUnavailableException(e);
        }
    }

}