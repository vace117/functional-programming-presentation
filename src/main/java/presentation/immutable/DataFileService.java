package presentation.immutable;

import java.io.File;

import presentation.composition.imperative.DataFileMetadata;
import presentation.laziness.DataType;
import presentation.mutable.User;


public class DataFileService {
    private UserService    userService;
    private DataFileWriter writer;

    // This implementation makes it impossible for DataFileMetadata
    // to exist in an invalid state. If we return from this method,
    // we can trust that DataFileMetadata is good unconditionally.
    //
    // It is now impossible to forget to initialize a field - compiler
    // won't let you!
    //
    public DataFileMetadata createDataFileMetadata(User     user, 
                                                   DataType type, 
                                                   String   contents, 
                                                   String   location) {

        return new DataFileMetadata(
            userService.attachCustomerInfo(user, type),
            contents == null ? DataType.EMPTY_TYPE : type,
            writer.writeDataFile(contents, location)      
        );
    }
    
    public DataFileMetadata updateFile(DataFileMetadata meta) {
        return meta.withFile( _createNewContents() );
    }
    
    private File _createNewContents() {
        return null;
    }

}


