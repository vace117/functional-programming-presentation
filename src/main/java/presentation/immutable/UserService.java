package presentation.immutable;

import presentation.laziness.DataType;
import presentation.mutable.CustomerDAO;
import presentation.mutable.User;

public class UserService {

    private CustomerDAO dao;

    public Long attachCustomerInfo(User user, DataType type) {
        if (dao.isSupportedType(user, type)) {
            return dao.getCustomerId(user);
        }
        else {
            throw new IllegalArgumentException("Badness");
        }
    }

}
