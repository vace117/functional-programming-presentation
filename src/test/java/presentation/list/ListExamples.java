package presentation.list;

import static org.junit.Assert.assertEquals;
import static presentation.util.Utils.print;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import io.vavr.collection.List;
import io.vavr.control.Try;
import presentation.domain.Example1;

public class ListExamples {
    
    List<Example1> theList = List.of(
        new Example1(1, "One"),
        new Example1(2, "Two"), 
        new Example1(3, "Three")
    );

    @Before
    public void setup() {
        print("Original: \n  " + theList + "\n");
    }
    
    @Test
    public void demoMap() {
        // Map objects to their numbers
        //
        print("Just Numbers: \n  " + 
            theList.map(Example1::getNumber)
        );

        // Change some field on the object. 
        // Note that we are creating new instances, b/c Example1 is immutable
        //
        print("Times 10: \n  " + 
            theList.map( e -> e.withNumber(e.getNumber() * 10) )
        );
    }
    
    @Test
    public void demoFilter() {
        // Find element with specific condition
        //
        print("Found the thing: \n  " + 
            theList
                .find(e -> e.getNumber() == 7)
                .getOrNull() 
        );
        
        // Find multiple elements with specific condition
        //
        print("\nFiltered list: \n  " + 
            theList.filter(e -> e.getNumber() > 1)
        );
    }
    
    @Test
    public void demoFold() {
        // Sum all the values in a list
        //
        print("The sum is: " +
            theList
                .map(Example1::getNumber)
                .fold(0, (n1, n2) -> n1 + n2)
        );
        
        // Multiply each number by 2, convert to String and 
        // produce a total string appending the results together
        //
        print("All doubled numbers: " +
            theList.foldLeft("", (str, e) -> str + "_" + (e.getNumber() * 2))
        );
    }
    
    @Test
    public void demoSideEffect_withUncheckedException() {
        // Perform a side-effect on each element. If a RuntimeException is thrown here,
        // we have to remember to handle it, which we usually forget to do!
        // Even if we handle it, the control flow of the program is interrupted, as soon
        // as the error occurs.
        //
        theList.forEach(e -> e.saveToDatabase());
    }
    
    @Test
    public void demoSideEffect_CheckedException() {
        // Even if we are dealing with a side-effect, we can avoid using 'forEach', if we approach
        // our error handling functionally. This is much better than just catching an exception,
        // b/c we get a List<Try> with results for each thing we persisted. This means that we can 
        // have much more sophisticated error handling and reporting.
        //
        print("Successfully persisted objects: " + 
            theList
                .map(e -> Try.run      ( ()   -> e.saveToDatabase_withCheckedException() )
                             .onFailure(error -> _handleError(error, e))
                )
                .filter( t -> t.isSuccess() ) // See how many records succeeded
                .size()
        );
    }

    /**
     * Super sophisticated error-handling goes on here
     */
    private void _handleError(Throwable error, Example1 record) {
        print("Oh noes! " + record + " could not be persisted: " + error);
    }
    
    
    private ConfigObject config;
    private JdbcTemplate jdbcTemplate;
    
    
    
    protected void verifyGrantsAndSynonymsForSchema(SchemaInfo schema, int expectedNumberOfTables) {
        String errors = config.getAllUsersForSchema(schema).map( user ->
            Try.run( () -> {
                int synonymCount = jdbcTemplate.queryForObject(
                    String.format("select count(synonym_name) from all_synonyms where owner = '%s'", user.username), 
                    Integer.class
                );
                
                assertEquals(
                    String.format("User '%s' doesn't have the correct number of synonyms!", user.username), 
                    expectedNumberOfTables, synonymCount
                );
            })
        )
        .filter(     t           -> t.isFailure())
        .map   (     t           -> t.getCause().getMessage())
        .fold  ("", (err1, err2) -> err1 + "\n" + err2); 
        
        if ( !StringUtils.isEmpty(errors) ) {
            throw new AssertionError(errors);
        }
    }
    
    
    
    private class JdbcTemplate {
        public <T> T queryForObject(String query, Class<T> clazz) { return null; }
    }
    private class ConfigObject {
        public List<UserInfo> getAllUsersForSchema(SchemaInfo schemaInfo) {return null;}
    }
    
    private class UserInfo {
        public String username;
    }
    
    private class SchemaInfo {
        
    }
    

}
