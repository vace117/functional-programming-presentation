package presentation.optional;

import static presentation.util.Utils.print;

import org.junit.Before;
import org.junit.Test;

import io.vavr.collection.List;
import io.vavr.control.Option;
import presentation.domain.Example1;

public class OptionalExamples {
    
    List<Example1> theList = List.of(
        new Example1(1, "One"),
        new Example1(2, "Two"), 
        new Example1(3, "Three")
    );

    @Before
    public void setup() {
        print("Original: \n  " + theList + "\n");
    }
    
    /**
     * There are many ways of using an Option. Depends on what we are trying to do...
     */
    @Test
    public void demoOption() {
        // Just get the value, but throw an exception if it's not there
        //
        print("Element search result: " + 
            theList
                .find(e -> e.getNumber() == 666) // Search for non-existent element
                .getOrElseThrow( () -> new IllegalStateException("Woa! There was no element!") )
        );
    
        // Get the description of the value, or some default if not there
        //
        print("Element search result: " + 
            theList
                .find(e -> e.getNumber() == 666) // Search for non-existent element
                .map(Example1::getDescription)   // Optional is a functor, just like the list, so we can map!
                .getOrElse("NO RESULT!")
        );
        
        // Perform side-effects with the value
        //
        theList
            .find   (  e -> e.getNumber() == 666) // Search for non-existent element
            .peek   (  e -> print("Doing side-effecty stuff with " + e))
            .onEmpty( () -> print("Nothing was found!") );
    }

}
