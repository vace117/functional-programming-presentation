package presentation.lazyness;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import presentation.laziness.DataFileMetadataLazy;

public class LazyTests {
    
    @Test
    public void testLaziness() {
        DataFileMetadataLazy metadata = new DataFileMetadataLazy();
        
        String c1 = metadata.getContents();
        String c2 = metadata.getContents();
        
        assertTrue(c1 == c2);
    }

}
